#ifndef SLOAD_H
#define SLOAD_H

#include "common.h"

//////////////////////////////////////////////////////////////////////////////////////////
// Bumped surface loader                //
//////////////////////////////////////////////////////////////////////////////////////////
struct        surface_bumped
{
        half4         	base;
        half3         	normal;
        half         	gloss;
        half         	height;
};

half4   tbase                 (float2 tc)        {
        return                 tex2D                (s_base,         tc);
}


#ifdef         USE_PARALLAX
surface_bumped                sload_i         ( p_bumped I)        // + texld, mad, nrm(3), mad  = 1+1+3+1 = 6, 15+6=21, OK
{
        surface_bumped      S;
        half        height	=       tex2D      (s_bumpX, I.tcdh).w;                                //
                    height  =       height*parallax.x + parallax.y;                        //
        float2		new_tc  =       I.tcdh + height*normalize        (I.eye);                //
        half4       Nu      =       tex2D		(s_bump,         new_tc);                // IN:  normal.gloss
        half4       NuE     =       tex2D      	(s_bumpX,       new_tc);                // IN:         normal_error.height
        S.base              =       tbase                (new_tc);                                // IN:  rgb.a
        S.normal            = 		Nu.wzyx + (NuE.xyz - 1.0h);							 // (Nu.wzyx - .5h) + (E-.5)
        S.gloss             =       Nu.x	;                                        //        S.gloss             =        Nu.x*Nu.x;
        //S.gloss           =       Nu.x*Nu.x	;                                        //        S.gloss             =        Nu.x*Nu.x;

        S.height            =       NuE.z       ;

	#ifdef USE_TDETAIL
        half4       detail  =		tex2D(s_detail,I.tcdbump)        	;
        S.base.rgb          =		S.base.rgb     * detail.rgb*2		;
        S.gloss             =  		S.gloss * detail.w * 2				;
	#endif//USE_TDETAIL

        return                S;
}
#else //USE_PARALLAX
surface_bumped                sload_i         ( p_bumped I)
{
        surface_bumped        S;
        half4 Nu 			=		tex2D                (s_bump, I.tcdh);                        // IN:  normal.gloss
        half4 NuE           =		tex2D                (s_bumpX,I.tcdh);                        // IN:         normal_error.height
        S.base              =		tbase                (I.tcdh)		;                         // IN:  rgb.a
        S.normal            =		Nu.wzyx + (NuE.xyz - 1.0h)			;
        S.gloss             =		Nu.x							;                         //        S.gloss             =        Nu.x*Nu.x;
        S.height            = 		NuE.z;

	#ifdef USE_TDETAIL

	#ifdef USE_TDETAIL_BUMP
	half4 NDetail		= tex2D( s_detailBump, I.tcdbump);
	half4 NDetailX		= tex2D( s_detailBumpX, I.tcdbump);
	S.gloss				= S.gloss * NDetail.x * 2;
	S.normal			+= NDetail.wzy + NDetailX.xyz - 1.0h; //	(Nu.wzyx - .5h) + (E-.5)
	half4 detail		= tex2D( s_detail, I.tcdbump);
	S.base.rgb			= S.base.rgb * detail.rgb * 2;
	#else //USE_TDETAIL_BUMP

        half4 detail		=        tex2D(s_detail,I.tcdbump)    ;
        S.base.rgb          =      	S.base.rgb*detail.rgb        	*2      ;
        S.gloss             =  		S.gloss * detail.w * 2			;
	#endif //USE_TDETAIL_BUMP

	#endif //USE_TDETAIL

        return              S;
}
#endif //USE_PARALLAX


surface_bumped		sload	 	( p_bumped I)
{
	surface_bumped	S 	= sload_i(I);

	#ifdef USE_SHARP_BUMP
	//�� �� �������� ������� �����. ��� ������, ��� ������� ������.
	S.normal.z *=	0.5; //. make bump twice as contrast (fake, remove me if possible)
	// S.normal.z *=0.25;//�������
	#endif //USE_SHARP_BUMP

	return	S;
}

#endif//#ifndef SLOAD_H
